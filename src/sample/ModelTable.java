package sample;

public class ModelTable {
    private String nazwa;
    private String kalorie;
    private String bialko;
    private String weglowodany;
    private String tluszcze;

    public ModelTable(String nazwa, String kalorie, String bialko, String weglowodany, String tluszcze) {
        this.nazwa = nazwa;
        this.kalorie = kalorie;
        this.bialko = bialko;
        this.weglowodany = weglowodany;
        this.tluszcze = tluszcze;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getKalorie() {
        return kalorie;
    }

    public void setKalorie(String kalorie) {
        this.kalorie = kalorie;
    }

    public String getBialko() {
        return bialko;
    }

    public void setBialko(String bialko) {
        this.bialko = bialko;
    }

    public String getWeglowodany() {
        return weglowodany;
    }

    public void setWeglowodany(String weglowodany) {
        this.weglowodany = weglowodany;
    }

    public String getTluszcze() {
        return tluszcze;
    }

    public void setTluszcze(String tluszcze) {
        this.tluszcze = tluszcze;
    }

}
