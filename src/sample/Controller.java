package sample;

import java.sql.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Controller {

    // Dzienniczek.fxml section
    @FXML
    public TextField ilosc;
    @FXML
    public TextField nazwaProd;
    @FXML
    private PieChart wykresik;
    @FXML
    private Button generate;
    @FXML
    private ProgressBar progresKalorii;
    @FXML
    private Label kl;

    // Produkty.fxml section
    @FXML
    private TableView<ModelTable> table;
    @FXML
    private TableColumn<ModelTable, String> col_nazwa;
    @FXML
    private TableColumn<ModelTable, String> col_kalorie;
    @FXML
    private TableColumn<ModelTable, String> col_bialko;
    @FXML
    private TableColumn<ModelTable, String> col_weglowodany;
    @FXML
    private TableColumn<ModelTable, String> col_tluszcze;
    @FXML
    private javafx.scene.control.TextField wzrost;
    @FXML
    private javafx.scene.control.TextField nazwa;
    @FXML
    private javafx.scene.control.TextField kalorie;
    @FXML
    private javafx.scene.control.TextField bialko;
    @FXML
    private javafx.scene.control.TextField weglowodany;
    @FXML
    private javafx.scene.control.TextField tluszcze;

    // BMR.fxml section
    @FXML
    private javafx.scene.control.TextField wiek;
    @FXML
    private javafx.scene.control.TextField waga;
    @FXML
    private javafx.scene.control.TextField wynik;

    private Connection con = null;
    private PreparedStatement pst = null;
    private ResultSet rs = null;

    private final ObservableList<ModelTable> listaProduktow = FXCollections.observableArrayList();
    private final ObservableList<PieChart.Data> wykresKolowy = FXCollections.observableArrayList();

    /**
     * Shows actual system date.
     *
     * @return String with actual date
     */
    private String dataDzisiaj() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        return dateFormat.format(date);
    }

    /**
     * Adds product into database and table with specified name and macros.
     *
     * @param actionEvent
     */
    public void dodajProdukt(ActionEvent actionEvent) {
        try {
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            String query = "INSERT INTO produkty"
                    + "(Nazwa, Kalorie, Bialko, Weglowodany, Tluszcze)"
                    + "VALUES (?,?,?,?,?);";

            pst = con.prepareStatement(query);
            pst.setString(1, nazwa.getText());
            pst.setString(2, kalorie.getText());
            pst.setString(3, bialko.getText());
            pst.setString(4, weglowodany.getText());
            pst.setString(5, tluszcze.getText());
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,
                    "Dodano produkt!",
                    "Dodawanie",
                    JOptionPane.PLAIN_MESSAGE);

            listaProduktow.clear();
            wyswietlProdukty();

            nazwa.clear();
            kalorie.clear();
            bialko.clear();
            weglowodany.clear();
            tluszcze.clear();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Deletes product from table and database.
     *
     * @param actionEvent
     */
    public void usunProdukt(javafx.event.ActionEvent actionEvent) {
        try {
            String query = "DELETE FROM produkty WHERE nazwa=?";
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            pst = con.prepareStatement(query);
            pst.setString(1, nazwa.getText());
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,
                    "Usunięto produkt!",
                    "Usuwanie",
                    JOptionPane.PLAIN_MESSAGE);

            listaProduktow.clear();
            wyswietlProdukty();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Shows products in table fetched from database
     */
    public void wyswietlProdukty() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");
            ResultSet rs = con.createStatement().executeQuery("SELECT * FROM produkty;");
            while (rs.next()) {
                listaProduktow.add(new ModelTable(rs.getString("nazwa"),
                        rs.getString("kalorie"),
                        rs.getString("bialko"),
                        rs.getString("tluszcze"),
                        rs.getString("weglowodany")));
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }

        col_nazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        col_kalorie.setCellValueFactory(new PropertyValueFactory<>("kalorie"));
        col_bialko.setCellValueFactory(new PropertyValueFactory<>("bialko"));
        col_weglowodany.setCellValueFactory(new PropertyValueFactory<>("weglowodany"));
        col_tluszcze.setCellValueFactory(new PropertyValueFactory<>("tluszcze"));

        table.setItems(listaProduktow);
    }

    /**
     * Calculates and adds calories into daily calories demand at database
     *
     * @param ilosc grams of product
     */
    private float obliczWartoscKaloryczna(float ilosc) {
        float b = 0;
        float ww = 0;
        float t = 0;
        float calories = 0;

        try {
            String query = "SELECT bialko, weglowodany, tluszcze FROM produkty WHERE nazwa='" + nazwaProd.getText() + "';";
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            ResultSet rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                b = rs.getFloat("bialko");
                ww = rs.getFloat("weglowodany");
                t = rs.getFloat("tluszcze");
            }

            calories = (ilosc / 100) * ((4 * b) + (4 * ww) + (9 * t));

        } catch (Exception ex) {
            System.out.println(ex);
        }
        return calories;
    }

    /**
     * Adds products to calories set
     *
     * @param actionEvent
     */
    public void dodajSpozytyProdukt(ActionEvent actionEvent) {
        float calories = obliczWartoscKaloryczna(Float.valueOf(ilosc.getText()));
        try {
            String query = "INSERT INTO dzienniczek (data, kalorie) VALUES (?,?);";

            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            pst = con.prepareStatement(query);
            pst.setString(1, dataDzisiaj());
            pst.setFloat(2, calories);
            pst.executeUpdate();

            JOptionPane.showMessageDialog(null,
                    "Dodano do bilansu!",
                    "Bilans zaaktualizowany",
                    JOptionPane.PLAIN_MESSAGE);

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Searches given product
     *
     * @param actionEvent
     */
    public void wyszukajProdukt(ActionEvent actionEvent) {
        String n = "";
        float k = 0;
        float b = 0;
        float ww = 0;
        float t = 0;

        try {
            String query = "SELECT * FROM produkty WHERE nazwa='" + nazwa.getText() + "';";
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            ResultSet rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                n = rs.getString("nazwa");
                k = rs.getFloat("kalorie");
                b = rs.getFloat("bialko");
                ww = rs.getFloat("weglowodany");
                t = rs.getFloat("tluszcze");
            }

            Object[] w = {"Produkt: " + n,
                    "Kcal/100g: " + k,
                    "Bialko/100g: " + b,
                    "Wegle/100g: " + ww,
                    "Tluszcze/100g: " + t,
            };
            JOptionPane.showMessageDialog(null, w,
                    "Wynik wyszukiwania",
                    JOptionPane.PLAIN_MESSAGE);

            listaProduktow.clear();
            wyswietlProdukty();

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * Calculates how much calories users ate.
     *
     * @return value of eaten calories
     */
    private double wyswietlSpozyteMakro() {
        float makro = 0;
        String data = dataDzisiaj();

        try {
            data = data.substring(0, 10);
            String query = "SELECT SUM(kalorie) AS zjedzone FROM dzienniczek WHERE data LIKE'" + data + " %';";
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            ResultSet rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                makro = rs.getFloat("zjedzone");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return makro;

    }

    /**
     * Creates PieChart of eaten and missing calories.
     */
    public void wyswietlWykres() {
        double bmr = 0;
        double zjedzone;
        double brakujace;
        double pb;

        try {
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");
            ResultSet rs = con.createStatement().executeQuery("SELECT bmr FROM bmr;");
            while (rs.next()) {
                bmr = rs.getFloat("bmr");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        zjedzone = wyswietlSpozyteMakro();
        brakujace = bmr - zjedzone;

        wykresKolowy.add(new PieChart.Data("Brakujące kalorie", brakujace));
        wykresKolowy.add(new PieChart.Data("Spożyte kalorie", zjedzone));

        wykresik.setTitle("Bilans kalorii");
        wykresik.setData(wykresKolowy);

        kl.setText((int) zjedzone + " / " + (int) bmr + " kcal");

        pb = ((100 * zjedzone) / bmr);
        progresKalorii.setProgress(pb / 100);
        progresKalorii.setVisible(true);
    }

    /**
     * Creates stage reliable for Produkty tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void DoProdukty(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Produkty.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Creates stage reliable for BMR tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void DoBMR(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("BMR.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Creates stage reliable for Wykresy tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void DoDzienniczek(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Dzienniczek.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Calculates users BMR
     *
     * @return value of BMR
     */
    public float obliczBMR() {
        boolean gj = true;
        float bmr = 0;

        if (!wzrost.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            wzrost.setPromptText("Miałeś podać liczbę (dodatnią) ");
            gj = false;
        }

        if (!wiek.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            wiek.setPromptText("Miałeś podać liczbę (dodatnią)");
            gj = false;
        }
        if (!waga.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            waga.setPromptText("Miałeś podać liczbę (dodatnią)");
            gj = false;
        }
        if (gj) {
            float wzrost1 = Float.parseFloat(wzrost.getText());
            float wiek1 = Float.parseFloat(wiek.getText());
            float waga1 = Float.parseFloat(waga.getText());

            bmr = (float) (66 + (13.7 * waga1) + (5 * wzrost1) - (6.76 * wiek1));
        }
        return bmr;
    }

    public void kcalNaRedukcji(ActionEvent actionEvent) {
        float result = obliczBMR() - 300;
        insertBMR(result);
    }

    public void kcalNaMasie(ActionEvent actionEvent) {
        float result = obliczBMR() + 300;
        insertBMR(result);
    }

    public void kcalNaStagnacji(ActionEvent actionEvent) {
        float result = obliczBMR() + 0;
        insertBMR(result);
    }

    /**
     * Inserts users BMR into database to BMR table.
     *
     * @param result value of BMR calculated {@link #obliczBMR()}
     */
    private void insertBMR(float result) {
        DecimalFormat d = new DecimalFormat("#");

        try {
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/produkty", "root", "");

            String sqlInsert = "INSERT INTO bmr"
                    + "(bmr)"
                    + "VALUES (?);";

            pst = con.prepareStatement(sqlInsert);
            pst.setFloat(1, result);
            pst.executeUpdate();

            wynik.setText(String.valueOf(d.format(result)) + " kcal");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }


}
